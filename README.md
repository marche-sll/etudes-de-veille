# Présentation
[[_TOC_]]

# Les études de veille du marché de support au logiciel libre

L'objet de ce projet est de permettre la publication des études
de veille réalisées dans le cadre du marché de support logiciel
libre au bénéfice de l'administration française et piloté par la
DGFiP. Cette publication est opérée dans le cadre de ses missions
de service public.

Les études réalisées sur des sujets proposés par les
administrations peuvent être de nature stratégique avec comme
objectif de déterminer le niveau de maturité de l'offre en
logiciels libres sur un domaine fonctionnel précis, tandis que
les veilles plus techniques vont comparer point à point au moyen
d’une grille fonctionnelle les logiciels existants du domaine.

Au final Les conclusions d’une veille sont susceptibles de
motiver l’inclusion d'un logiciel dans le périmètre du marché de
support interministériel.

Chaque étude se présente sous la forme d'un
document au format bureautique ODT sous licence creative commons
[Attribution - Partage dans les Mêmes Conditions 2.0 France (CC
BY-SA 2.0 FR)](https://creativecommons.org/licenses/by-sa/2.0/fr/)


# Présentation des veilles publiées

## IA générative, état de l’art et solution d’aujourd’hui

Les percées récentes des systèmes dits d'"intelligence
artificielle" générative basés sur les LLM (Large Language
Model) ont marqué les esprits. Au-delà des annonces tonitruantes
de certains acteurs positionnés sur le domaine, l'objet de cette
étude est de faire le point sur ces technologies émergentes et
leur intérêt particulièrement dans le domaine du développement
informatique.

Même si les briques élémentaires nécessaires à la construction
de ces systèmes sont massivement sous licence libre, peu de
solutions clé en main peuvent se prétendre Open Source
(définition encore largement débattue et non abordée dans cette
étude) d'autant que beaucoup se présentent comme des services en
lignes avec souscription.

Si certains systèmes peuvent être opérés en interne pour des
questions de sécurité, les coûts en expertise et en moyen
matériel ne sont pas négligeables. Par ailleurs, ces
technologies présentent des risques spécifiques qu'il faut
anticiper.

- [EtudeSLL-IA_generative-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1164/download)

## Stockage documentaire en ligne
			
L'accès et le partage de documents en ligne via des plateformes
numériques sont devenus courants grâce à des solutions grand
public telles que Google Drive, OneDrive et Dropbox. Bien que le
besoin en la matière soit bien établi et que de nombreuses
solutions libres et souveraines semblent disponibles, peu
parviennent à répondre pleinement aux exigences fonctionnelles
professionnelles attendues, telles que la sécurité, le
chiffrement, la scalabilité et l'archivage.

Même si une solution comme SeaFile se fait remarquer par ses
performances et sa focalisation sur la quasiment seule
synchronisation de fichiers, Nextcloud et ownCloud (Infinite
Scale) se distinguent comme les solutions les plus adaptées pour
répondre à ces attentes sur le long terme et à des échelles
industrielles. Solides techniquement, les deux suites (qui
partagent le même socle initial de code), couvrent un large
spectre fonctionnel, offrent des possibilités d'évolution,
bénéficient d'une gouvernance solide et du soutien d'industriels
européens capables de répondre aux besoins en personnalisation
ou fonctionnels spécifiques.

- [EtudeSLL-stockage_documentaire-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1163/download)

## Outils de réservation de ressources

Des ressources très variées (Salles, matériels, véhicules...)
sont mises à disposition des agents pour l'exercice de leur
mission. Un outil de réservation va décrire les ressources,
présenter le planning des disponibilités, proposer leur
réservation, envoyer des notifications et des alertes de
rappels.

Trois solutions répondant aux exigences techniques et
fonctionnelles de l'administration ont été retenues pour cette
étude.

Easy!Appointments est une solution facile à utiliser et
personnalisable. Son interface moderne responsive fonctionne sur
ordinateur, smartphones et tablettes. Il permet une gestion
détaillée des rôles et des accès.

MRBS (Meeting Room Booking System) est une solution robuste
appuyée sur des composants éprouvés (PostgreSQL et annuaire
LDAP). Disposant d'une gestion fine des permissions, ses
fonctionnalités avancées incluent la gestion des réservations
ponctuelles et répétitives.

Booked Scheduler offre une interface utilisateur
particulièrement intuitive et ses outils de reporting avancés
facilitent la consultation et la gestion des créneaux
horaires. Le calendrier des réservations peut être intégré à des
calendriers externes de réunions Zoom par exemple.
			
- [EtudeSLL-reservation_de_ressources-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1138/download)
- [EtudeSLL-reservation_de_ressources-pub.ods](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1139/download)

## Intégration et orchestration de flux de données

L'objet de cette étude est de proposer une alternative à
TradeXpress maintenant obsolète, couvrant l'intégration et
l'orchestration de flux de données. Il apparaît que ce besoin ne
peut être couvert que par la combinaison de deux types d'outils.

Les solutions libres d'intégration de données considérées sont
Airbyte, n8n, Pentaho, Apache NIFI, Apache Hop, Meltano, DBT
Pipeli, neWise.

Les solutions libres d'orchestration de flux de données étudiées
sont pour leur part, Apache Airflow, Temporal, Rundeck, Prefect,
Dagster, Apache Dolphinscheduler

Il ressort de l'étude que la mise en oeuvre conjointe d'Apache
Hop et d'Apache AirFlow constitue une réponse intéressante pour
couvrir le périmètre attendu.

- [EtudeSLL-integration_de_donnees-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1137/download)

## Gestion de miroirs et de dépôts Linux

Le déploiement d'un environnement Linux repose sur la mise à
disposition de dépôts de paquets logiciels. Les paquets sont des
unités modulaires de logiciels contenant les fichiers
exécutables, leur script d'installation, leur paramétrage et la
liste des dépendances pour leur bon fonctionnement. Trois
grandes catégories d'outils peuvent être distinguées.

Les outils de mirroring simples comme apt-mirror et debmirror
qui réalise la mise en miroir au sens strict, c’est-à-dire en
conservant les index-files et les signatures d’origine. Ces
outils sont très simples à déployer, car ce sont des paquets
Linux standards.

Les outils de gestion de dépôts avec des fonctionnalités
supplémentaires comme reprepro qui s'inscrit dans la philosophie
du paquet toute en offrant davantage de fonctionnalités. Cet
outil est approprié dans un contexte de gestion de paquets
personnalisés.

Les outils mixtes "tout en un" aptly, repomanager ou Pulp
s'éloignent de la philosophie traditionnelle pour proposer des
solutions plus complètes et intégrées. Ces outils sont bien
adaptés aux besoins vastes, tels que le “mirroring”, la gestion
de paquets personnalisés, le support de plusieurs types de
paquets et d'autres exigences au-delà des simples paquets
Linux.

- [EtudeSLL-Miroirs_Linux-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1126/download)
- [EtudeSLL-Miroirs_Linux-pub.ods](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1127/download)

## Alternatives à l'ETL Talend

La société Talend à l'origine de l'ETL (Extract/Transorm/Load)
Talend Open Studio a été rachetée par la société QLIK
début 2023. Celle-ci, début 2024, a décidé de mettre fin à la
disponibilité de l'ETL sous licence libre. L'objet de l'étude
est d'identifier des alternatives libres.

L'alternative la plus évidente est Talaxie Open Studio, portée
par la société française Deilink qui se propose de continuer la
maintenance et l'évolution de l'ETL de Talend à partir des
dernières versions publiques du code. Cependant l'initiative est
encore fragile en attendant un modèle économique viable. Apache
Hop et Apache NIFI constituent des solutions intéressantes dans
une approche NoCode/LowCode.

La solution peut aussi consister à changer de perspective avec
les ELT (Extract/Load/Transform) qui se développe beaucoup ces
dernières années. Dremio Community, Trino ou Prestodb dans une
modalité NoCode/LowCode aussi sont intéressantes.

Dans une approche à base de code Python, Panda, Petl ou Bonobo
sont des frameworks à considérer. Avec le langage Java, il
faudra se tourner vers APache Camel. Enfin plus exotique
Ballerina est un langage conçu spécifiquement pour l'échange et
le traitement de données.

[EtudeSLL-Alternatives_Talend-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1125/download)

## Les éditeurs PDF
	
La suite Acrobat Reader publiée par la société Adobe à l'origine
du format PDF est la référence en matière d'édition
PDF. L'objectif de cette étude est d'analyser les alternatives,
en particulier celles sous licence libre.

Les éditeurs PDF étudiés sont Sejda PDF, PDFSam, PDFtk, Sumatra
PDF, Firefox Pdf Viewer, Chrome PDF Viewer, Okular, LibreOffice
Draw et Xournal++.

En plus des fonctionnalités classiques d'édition, une attention
particulière est portée sur la gestion de la signature
électronique.

Si aucune solution alternative ne rivalise en l'état avec
Acrobat Reader, les projets liés à l’écosystème Sejda méritent
une attention particulière pour leur niveau de fonctionnalité.
Des investissements communautaires utilement placés pourraient
permettre de combler l'écart afin d’offrir une alternative open
source au niveau des attentes des utilisateurs des secteurs
publics.

[EtudeSLL-Les_editeurs_PDF-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1111/download)

## Haute disponibilité des bases de données relationnelles 

La haute disponibilité des bases de données fait référence à la capacité d’une base de données à rester opérationnelle et accessible en permanence, même en cas de panne ou de défaillance. Le principe général consiste à déployer une base de données sur plusieurs serveurs avec des mécanismes de synchronisation et de bascules lorsqu'un serveur ne répond plus.

Les deux bases de données étudiées, PostgreSQL et MySQL/MariaDB dispose de solutions de haute disponibilité spécifique, PostgreSQL automatic failover, RepMgr et Patroni pour la première et Galera cluster, MaxScale/ProxySQL/HAProxyProxy + MRM et MySQL Master-HA pour MySQL/MariaDB.

Ces diverses solutions sont mises en perspectives avec la solution de haute disponibilité proposée par Oracle RAC.

Il ressort de l'étude que la haute disponibilité n'est plus un facteur discriminant pour le choix d'une base de données. PostgreSQL et MariaDB/MySQL proposent des solutions de haute disponibilité convaincantes.

[EtudeSLL-Haute-Disponibilites_BDD-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1110/download)

## Fiabilité des dépots logiciels

Les systèmes d’information de l’administration font appel à de
nombreux logiciels libres. Que ceux-ci soient provisionnés
depuis les dépôts de distributions Linux ou soient incorporés
lors du développement d’applications métiers, sous forme de
bibliothèques provenant de dépôts spécialisés (Dépôt Javascript
NPM, dépôt Java avec Maven Central, dépôt Python, dépôt PHP,
dépôt Perl CPAN….), se pose la question de la sécurité de ces
codes. Cette étude à deux objectifs :
- Déterminer les critères de qualification des dépôts de code :
identifier les standards de sécurité et de fiabilité
nécessaires pour approuver un dépôt. Les mécanismes de
signature et vérification de signature sont déterminants pour
sécuriser le provisionnement des paquets.
- Identifier les outils capables de tracer les dépendances
formaliser ou non par le SBOM (Software Bill of Materials) et
de scaner les vulnérabilités connues.

[EtudeSLL-Fiabilite_depots_logiciels-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1109/download)

## Tests automatisés 

La qualité des applications développées ne peut être assurée
sans la mise en place de batteries de tests automatisés à même
de garantir la conformité fonctionnelle des applications et la
non-régression au fil des versions.

Les solutions no-code qui permettent l'enregistrement des tests
directement en agissant sur l'interface de l'application sont
exclusivement propriétaires. Les solutions présentées sont
Leapwork, Agilitest et Ranorex.

De leur côté, les solutions présentées sous licence libre sont
toutes à base de script ; il s'agit de Robot Framework,
Playwright, Cypress, Selenium, Cucumber, Katalon, Behat,
Nightwatch.

Sous licence libre, Robot Framework se distingue par ses
facilitées de codage des tests, mais Playwright, Cypress et
Sélénium sont des alternatives sérieuses.

[EtudeSLL-Tests_Automatises-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1098/download)

## Solutions de proxy

Un serveur proxy est un intermédiaire entre les utilisateurs
d'un réseau et Internet. Il joue un rôle important dans la
gestion du trafic réseau, l'amélioration de la sécurité, et
l'optimisation de la performance. Les caractéristiques et
avantages d'un serveur proxy sont particulièrement pertinents
dans des environnements avec de grands sous-réseaux.

Les principales fonctionnalités attendues sont le contrôle
d'accès, la mise en cache des contenus, la Sécurité en masquant
les adresses du sous-réseau, l'audit et la surveillance des flux
réseau et enfin de l'équilibrage de charge.  L'étude s'intéresse
à trois solutions, TinyProxy, Apache Traffic Server et Squid.
			
Malgré la prédominance de Squid dans de nombreux scénarios
d'usage, notamment en raison de sa longue histoire et de sa
réputation de fiabilité, Apache Traffic Server gagne du terrain,
en particulier car il domine déjà sur le marché des serveurs web
et donc son adoption peut être plus aisée. Il se distingue par
sa capacité à optimiser la livraison de contenu et à gérer
efficacement le trafic réseau, ce qui en fait une alternative de
plus en plus considérée sur le marché des serveurs proxy.

[EtudeSLL-Proxy-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1097/download)

## Outils de gestion de projet

Pour aider la gestion de projet, des outils dédiés facilitent la
définition des tâches, leur ordonnancement, les ressources
affectées à ses tâches et finalement le suivi de leur
réalisation. C'est un outil au coeur de la collaboration de
l'équipe, dédiée à la réalisation du projet.

À la demande de l'administration, l'étude de veille se limite
aux deux logiciels libres Projeqtor et OpenProject, dans la
perspective d'une gestion de projet "classique" par opposition
aux méthodes agiles requérant des outils plus spécifiques de
type Kanban...

L’étude montre que les deux logiciels sont très complets avec
cependant une supériorité fonctionnelle sur le coeur de la
gestion de projet à l'avantage de Projeqtor. Toutefois,
OpenProject montre une belle dynamique qu'une adoption par
l'administration pourrait soutenir.

[EtudeSLL-Outils_Gestion_de_Projet-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1101/download)

## Gestion d'infrastructures virtualisées

La production informatique repose essentiellement sur des
infrastructures virtualisées et cloud. Des outils dédiés
permettent la mise en place et la gestion de ces
infrastructures.  L'objet de l'étude est de proposer un état des
lieux de l'offre Open Source, en se concentrant sur quatre
logiciels : OpenNebula, Proxmox VE, oVirt, Openstack. Ils sont
analysés sur le plan de la gestion de l'infrastructure, des
services, de l'orchestration, du monitoring, de la sécurité, des
interfaces, des hyperviseurs, des réseaux externes et stockages
externes supportés.

Il ressort qu'au regard du besoin exprimé, OpenNebula constitue
un bon compromis en termes de simplicité, flexibilité et
efficacité entrent l'exhaustivité d'OpenStack au prix d'une
certaine complexité que l'on retrouve avec oVirt. Proxmox est
une alternative intéressante à OpenNebula quoique légèrement
plus complexe à mettre en oeuvre.

[EtudeSLL-gestion_infrastructure-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1094/download)

## Solutions de CRM

L'objet de cette étude est d’élargir l'étude consacrée à suite
CRM aux autres solutions de gestion de la relation client
(Customer Relationship Management). Centrée sur les cinq
fonctionnalités principales attendues pour une solution de CRM,
à savoir, création d'une fiche contact, Création d'événements,
Possibilité de faire des recherches par mot-clef, gestion fine
des habilitations au sein du CRM et possibilité de faire des
retours statistiques/pilotages sur la création des contacts et
événements.

Quatre solutions libres ont été étudiées et comparées, SuiteCRM,
EspoCRM, Axelor CRM et Odoo CRM. Fonctionnellement, elles sont
très proches et couvrent les attentes fonctionnelles. L'ergonomie 
peut permettre de distinguer les solutions. Des ateliers avec les
utilisateurs peuvent aider à déterminer la meilleure solution. 
Sur le plan plus technique :

- Axelor CRM est développée en Java dans une approche «no-code» avec des fichiers de configurations XML ;
- Odoo est développé en Python. C’est la référence des solutions OpenSource de CRM ;
- EsproCRM et Suite CRM sont en PHP ; elles paraissent en deçà des autres solutions en termes de dynamisme de l’écosystème ;

[EtudeSLL-Outils_CRM-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1093/download)

## Reverse-proxy

Un Reverse proxy (serveur mandataire inverse) est un équipement
réseau positionné entre internet et un sous-réseau d'entreprise.
Il apporte sécurité, performance et simplicité de gestion de
l'infrastructure.
			
Dans ce domaine, les solutions Open Source sont nombreuses et
diversifiées ; l'étude présente six solutions.
 
- Caddy est une solution privilégiant la simplicité sans négliger les fonctionnalités.
- Traefik est particulièrement conçu pour les environnements conteneurisés, il est aussi capable de faire de l'équilibrage de charge.
- HAProxy réputé pour ses performances en plus de fonctions de reverse proxy, propose de l'équilibrage de charge et mise en cache intelligente.
- Envoy est performant et extensible porté par la Cloud Native Computing Foundation (CNCF) et donc dédié aux environnements cloud, mais sans exclusive.
- Apache HTTP Server que l'on ne présente plus, peut être paramétré en reverse proxy, mais ce n'est pas sa spécialité.
- NGinx le serveur web excelle aussi en tant que reverse proxy en raison de sa haute performance, de sa flexibilité de configuration, de sa capacité à gérer efficacement les requêtes statiques et dynamiques, ainsi que de sa robustesse en termes de tolérance aux pannes et de gestion de la charge.

[EtudeSLL-Reverse_Proxy-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1092/download)

## Bureautique en ligne

Depuis quelques années, l'édition de textes, de tableaux de présentations n'est plus l'apanage d'un client lourd installé sur le poste de l'utilisateur, mais peut être appréhendée au travers du navigateur, sans qu'il soit nécessaire de stocker les documents localement. Outre le stockage centralisé des documents, l'édition en ligne ouvre la possibilité de nouveaux usages, tout particulièrement la co-édition. 

L’objet de l’étude est de faire une analyse fonctionnelle, technique et financière des suites bureautiques libres en ligne Collabora Online et OnlyOffice sans oublier leur écosystème.

L’étude est menée en plusieurs temps : la méthode QSOS (Qualification et de Sélection de logiciels Open Source) est d’abord employée pour analyser la qualité de Collabora Online et OnlyOffice selon des critères objectivés. Ensuite est menée une analyse de l’écosystème économique, financier, technique et sur le plan de l’accessibilité. L'étude se conclut par une analyse fonctionnelle comparative des deux suites.

[EtudeSLL-Bureautique_en_ligne-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1072/download)

## Solutions de sauvegarde

Essentiel pour préserver un système d'information, la sauvegarde est le processus consistant à copier, mettre en sécurité puis restaurer les données suite à des incidents matériels ou logiciels. La nature des fichiers sauvegardés, la fréquence, et la sécurité des sauvegardes sont déterminantes dans la stratégie de sauvegarde - sans oublier le test des sauvegardes !

Beaucoup de solutions libres existent chacune avec sa logique propre ; le contexte dictera largement le meilleur choix. Parmi les solutions les plus génériques, c'est Bareo qui ressort pour sa complétude fonctionnelle et sa maturité. Bacula équivalente fonctionnellement est cependant moins communautaire (ce qui a motivé le fork Bareo en 2010). La toute jeune solution Kopia déjà fonctionnellement très riche est à surveiller.

Pour des besoins plus spécifiques, sans administration centralisée, les plus efficaces sont Borg ou encore Restic. 

[EtudeSLL-Sauvegarde-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1108/download)

## Robotic Process Automation (RPA)

Les outils de Robotic Process Automation permettent d’automatiser
des tâches répétitives et pour certains d’entre eux, proposent
d’enregistrer des suites d’interactions homme-machine, ce qui
donne la possibilité à des utilisateurs non experts de mettre en
place des robots réalisant leur processus métier.

Le panorama de ces outils est très vaste, tandis que le marché des
outils open source est plus restreint. Trois outils se distinguent
particulièrement, en raison de leurs fonctionnalités, de la
présence d’une documentation, d’une communauté et d’un écosystème
économique.

UIPath est un outil complet assorti d’une offre Saas éditeur. Si
le projet est open source au sens où le code est accessible, il ne
propose par de gouvernance claire ni ouverte. Il est porté par un
seul acteur économique (de très grande taille). Il propose
l’enregistrement des actions utilisateur ; son langage de
développement est .NET. UIPath était en 2019 le leader du marché.

OpenRPA est un logiciel intermédiaire bien documenté et actif qui
permet l’enregistrement d’actions. Il repose cependant
principalement sur un ou deux contributeurs ce qui fait peser un
risque sa pérennité.

Robocorp est un projet open source porté par une fondation qui
structure un écosystème économique. La gouvernance du projet est
claire, le périmètre fonctionnel de l’outil est large, il est
extensible en Python, mais ne ne propose pas l’enregistrement des
actions par les utilisateurs.

[EtudeSLL-RPA-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1043/download)

## Gestion de tickets

Un outil de ticketting permet de gérer la relation entre les
utilisateurs d'une ressource et l'équipe en charge de la
ressource. Ce domaine est largement couvert avec une offre open
source importante.

OSTicket est une solution simple qui couvre les fonctionnalités
attendues, mais son interface peut apparaître désuète.

Les solutions allemandes Zammad et Znuny quant à elles, sont plus
développées en termes de workflow et de personnalisation. Elles
demanderont cependant un investissement humain plus important pour
le paramétrage.

La solution de CMDB Itop propose une solution complète et très
intéressante avec sa notion de gestion d’impact lié au
ticketing. La modularité de son modèle de données est une force
que les autres outils n’ont pas. Sa mise en place demande un
investissement technique non négligeable pour les grands
organismes.

Les solutions les plus complètes sont GLPI et son fork récent
ITSM::NG, ce dernier répondant au manque d'écoute de la communauté
de la part de l'éditeur de GLPI. De nombreux modules permettent
d'adapter et étendre les fonctionnalités aux besoins métiers des
utilisateurs.

[EtudeSLL-Gestion_Tickets-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1042/download)

## Solutions d'E-Parapheur

Un processus administratif incontournable est la circulation de
documents pour relecture, visa et signature. Le parapheur
électronique transpose de manière dématérialisée ce type de
processus.  Les workflows de circulation peuvent être
relativement complexes, avec des mécanismes de
délégation. Différent niveau de signatures sont possible de
simple, avancé à qualifié.

Cinq parapheurs électroniques sont étudiés. Certains
exclusivement en mode SaaS et facile à mettre en oeuvre comme
FAST Parapheur ou Luxtrust COSI peuvent cependant poser des
problèmes de souverainetés. La solution Lex Enterprise bien
qu'en mode Saas permet un mode hybride avec conservation des
documents “On Premise”, la signature porte sur une empreinte des
documents.

Maarch Parapheur est une solution libre complète permettant la
signature et l'annotation à main levée, la signature déléguée
sans divulgation.

De même Libriciel iparapheur accepte les signatures CAdES, PAdES
et XAdES. Elle est conforme aux procédures dématérialisées des
administrations : pièces de marché public, bordereaux PESv2,
actes administratifs divers, documents RH, courriers, notes
internes, etc.

[EtudeSLL-Parapheurs-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1041/download)

## Tour d'horizon des outils de supervision
Un système d'information est constitué d'un très grand nombre de
composants logiciels dont le dysfonctionnement d'une seul peut
avoir des répercussions en cascades. Disposée d'une supervision
de chacun des éléments du SI est donc essentielle.

Cette étude extensive passe en revue une vingtaine de solutions
certaines constituant une solution globale tandis que d'autres
doivent être mises en association.

Aujourd’hui, Zabbix et Centreon continuent d’être des valeurs
sûres de la supervision ; Zabbix pour la vitalité de sa
communauté et Centreon pour sa simplicité de mise en œuvre avec
ses packs de plug-ins (dans sa version commerciale).
		
Pour couvrir les nouveaux besoins, liés aux conteneurs et aux
services managés, Prometheus (déclencheur à la seconde) est bien
plus approprié.  À noter que l’arrivée récente de Zabbix 6 (en
temps réel) pourrait lui permettre de rester dans la course du
cloud. Il est en effet orienté scalabilité et interfaçable avec
Prometheus ou même directement sur un Kubernetes.

L’analyse des données et leur mise à disposition à des
utilisateurs peuvent se faire avec Grafana. Globalement, toutes
les stacks modulaires construites autour de Grafana apportent
une vraie richesse fonctionnelle avec notamment la facilité
d’injection des logs avec Loki.

[EtudeSLL-Supervision-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1040/download)

## Catalogue de données

Un catalogue de données permet de créer et gérer un inventaire
organisé et explorable des données métier de l’entreprise. C'est
un outil essentiel pour la gouvernance, la sécurité et l'accès
aux données.

Les fonctionnalités attendues d'un catalogue de données sont de
trouver les données, de les comprendre, de les compléter, de les
organiser, de piloter leur qualité et de faciliter le travail
collaboratif sur ces données.

Si les solutions propriétaires étudiées sont sensiblement les
plus avancées fonctionnellement en particulier la solution Erwin
Data Catalog, les solutions libres sont bien présentes sur le
domaine. Six solutions sont ainsi étudiées. C'est
OpenDataDiscovery et OpenMetaData qui sont les plus avancées
fonctionnellement. Très proche fonctionnellement,
OpenDataDiscovery est la plus faible en ce qui concerne son
écosystème très lié à l'éditeur tandis que OpenMetaData présente
une activité communautaire plus riche et varié.

[EtudeSLL-datacatalogue-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1031/download)

## Qualité des données
La qualité des données se caractérise par sept facteurs :
fraîcheur, conformité, cohérence, complétude, exactitude,
unicité, interopérabilité. Les solutions objet de l'étude ont
pour fonction de mesurer et d'améliorer la qualité des
données. Les principales fonctions attendues sont donc :
- La détection des anomalies ;
- La normalisation et le nettoyage des données ;
- L'enrichissement des données ;
- La mesure de la qualité des données.

Six solutions propriétaires et cinq solutions libres sont
étudiées. L'offre propriétaire apparaît dominée par Informatica
Cloud Data Quality qui est une solution très complète et
puissante toutefois il s'agit d'un service cloud ne pouvant
répondre aux contraintes de souveraineté de certains.

L'alternative libre la plus complète est certainement Knime
couvrant l'essentiel du besoin avec une interface graphique de
programmation nécessitant une certaine technicité.

À noter que Talend Data Quality n'est plus libre depuis le
début 2024.

[EtudeSLL-dataquality-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1030/download)

## SuiteCRM et messagerie
SuiteCRM est une application de gestion de la relation client
(Customer Relationship Management) identifiée pour certains
besoins de l'administration. Après un rappel des fonctionnalités
classiquement attendues de ce type d'application, l'étude
explore les différents moyens d'établir un lien étroit avec la
gestion des contacts et la messagerie Outlook, sachant que
l'extension Outlook (payante) proposée par SuiteCRM est loin de
couvrir le besoin.

Les technologies d'extension Outlook VSTO et JavaScript sont
l'une et l'autre envisageables pour établir ce lien. VSTO est la
technologie la plus riche permettant l'accès fin à l'ensemble
des fonctionnalités Outlook. C'est cependant la plus complexe à
mettre à oeuvre. La technologie JavaScript est plus simple à
opérer, mais ne permettra pas de couvrir complètement les
fonctionnalités attendues. Ce type de chantiers (connecteurs et
synchronisation Outlook) sont des chantiers complexes et
coûteux.

[EtudeSLL-SuiteCRM-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/1029/download)

## Authentification basée sur les risques
L'authentification est une étape cruciale de l'accès aux services en ligne des administrations. Le renforcement de la	sécurité lors de cette étape ne se fait pas sans contrainte pour l'utilisateur. L'objet de cette veille est d'étudier les	dispositifs de scoring permettant d’adapter la cinématique d’authentification aux risques identifiés. Ainsi une connexion	depuis un lieu géographique inhabituel entraînera l'envoi d'un mot de passe à usage unique sur le téléphone de l'utilisateur.

Compte tenu du contexte de l'administration, il n'existe pas de	solution sur étagère. Cependant certaines fonctions natives de LemonLDAP::NG combinées avec d’autres outils Open Source comme	IP2location et Crowdsec permettent de couvrir le besoin.

Le logiciel Crowdsec est une solution libre de détection de	comportements malveillants et de blocage d’accès à différents niveaux (infrastructure, applications). L'outil s'appuie en	particulier sur une base centralisée d'adresses IP alimentée par la communauté.

IP2Location est un service de la société Hexasoft Development	Sdn. De nombreuses bases sont proposées dont le tarif dépend du	nombre d’informations fournies : pays, région, ville,	fournisseur internet, latitude, longitude, code postal, fuseau horaire, vitesse de connexion, météo, catégorie...

[EtudeSLL-Scoring_Authentification-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/953/download)

## Solutions de prototypage de site web

Lors de la conception des applications web, un outil de prototypage facilite les échanges entre les développeurs et le métier voir permet à ce dernier de définir les écrans de l'application. Idéalement le prototypage ainsi obtenu peut être	converti en code exploitable par les développeurs. L’outil peut	aussi permettre d'anticiper très en amont les principes d’accessibilité. 

En alternative aux solutions propriétaires, Figma ou Axure, les	solutions GrapesJS, craft.js, Saltcorn et Editor.js ont été	étudiées.GrapesJS se détache sensiblement comme une solution extensible	permet de créer ses propres layout et l'ajout de composants	personnalisés le tout avec une bonne simplicité d'utilisation.
			
Cependant CraftJS, malgré une documentation perfectible, est à privilégier pour des applications ReactJS en permettant l'importation directe des composants ReactJS. Éventuellement en contexte ReactJS on peut aussi utiliser Saltcorn qui encapsule	craft.js et ajoute des fonctionnalités pour gérer une	application dans son ensemble.

[EtudeSLL-Solutions_de_prototypage_de_site_web.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/874/download)

## Alternative à MECM

Édité par la société Microsoft, MECM est un logiciel de gestion centralisé d'un parc informatique permettant l'inventaire matériel et logiciel, la gestion des mises à jour et la gestion de la conformité et de la politique de sécurité. Quatre alternatives libres sont étudiées :

- **Rudder** (société Normation) sur un modèle Open Core propose une solution très aboutie couvrant l'ensemble des attentes de l'administration. Une montée en compétence et une phase d'initialisation importante sont toutefois nécessaires.
- **Pulse XMPP** (société Siveo) offre une bonne couverture des fonctionnalités attendues. Pour améliorer les fonctionnalités d'inventaire, Pulse peut être couplet à un outil tiers (GLPI / ITSM-ng / OCS Inventory).
- **OCS Inventory** est un projet historiquement conçu au sein d'une administration et porté maintenant par la société FactorFX. Le projet est une référence en matière d'inventaire. Il peut paraître un peu limité en matière de reporting, mais il est tout à fait possible de lui adjoindre un outil comme Grafana pour des tableaux de bord à façon.
- **GLPI / ITSM-NG** : GLPI très utilisé au sein des administrations est porté par la société Teclib tandis que son fork ITSM-NG est porté par FactorFX et Worteks. Les deux projets sont proches fonctionnels et se distinguent par l'offre de support proposé par chacune des sociétés. En  matière de déploiement il est nécessaire d'ajouter un agent de déploiement qui peut être celui de Fusion Inventory ou d'OCS Inventory.

[EtudeSLL-Alternative_MECM-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/694/download)

## Solutions de VPN

Un VPN (Virtual Privacy Network) est un dispositif permettant d'établir une connexion sécurisée entre un ordinateur et un réseau distant au moyen d'un réseau public. Les VPN s'appuient principalement sur trois protocoles : SSL/TLS, IPSec et Wireguard. Parmi les sept solutions étudiées, trois sortent du lot :

**OpenVPN** est la plus ancienne et la plus éprouvée ; à ce titre une large expertise est disponible pour accompagner sa mise en place. Toutefois OpenVPN n'est pas la solution la plus performante. 

**WireGuard** : est une jeune solution prometteuse, particulièrement performante au regard d'OpenVPN. Cependant sa mise en oeuvre sur un parc à l'échelle d'une administration ne sera pas facile faute d'outils d'exploitation et d'administration. L'offre TailScale comble ce vide.
			
**Pritunl** est probablement la solution répondant le mieux aux attentes de l'administration. Multi-protocole (OpenVPN, Wireguard et IPSec) elle dispose aussi d'une interface d'administration et de gestion intuitive. Un point de vigilance, il s'agit d'une solution encore jeune.

[EtudeSLL-VPN-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/692/download)

## Autorité de certification

La sécurité des données qui circulent sur internet repose pour l'essentiel sur le chiffrement par clé publique. Cela nécessite une infrastructure à clé publique ou PKI (Public Key Infrastructure) opéré par une autorité de certification.
L'objet de l'étude est de présenter les solutions sous licence libre couvrant les fonctionnalités attendues d'une autorité de certification d'entreprise.

Aucune des trois solutions étudiées EJBCA, Dogtag Certificat System et OpenXPKI ne couvre dans sa version libre l'ensemble des fonctionnalités - en particulier l'enrôlement des postes Windows. EJBCA se distingue par sa documentation, tandis que Dogtag est le produit qui à la fois propose le plus grand nombre de fonctionnalités, et qui demande le moins d'effort de configuration.

Tous disposent d'un système de plug-ins intégrés au cœur de leurs fonctionnalités tel le workflow d'émission de certificat, ce qui permet facilement d'adapter le dispositif aux spécificités de l'entreprise . Enfin, les trois projets présentés sont tous activement maintenus, mais majoritairement par leur éditeur.

[EtudeSLL-PKI-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/421/download)

## L'OpenJDK 17

Le langage Java depuis une vingtaine d'années fait partie du trio des langages de programmation les plus utilisés. Le JDK est l'élément essentiel pour mettre en oeuvre les programmes Java, puisqu'il intègre en particulier un compilateur et une machine virtuelle pour exécuter le bytecode produit par la compilation. L'implémentation la plus populaire qui est aussi l'implémentation de référence, c'est l'OpenJDK.

L'objet de cette étude est dans un premier temps d'analyser la solidité de la communauté en charge de l'évolution et de la maintenance de l'OpenJDK. Avec l'initiative Adoptium soutenue par un grand nombre d'acteurs majeurs dans le cadre de la fondation Eclipse, la pérennité du projet est acquise.

Dans un deuxième temps, l'étude présente les évolutions techniques les plus notables de la dernière version LTS (Long Term Support), à savoir la 17 par rapport à la version 11. Autant la version 11 apparaissait comme une version de rupture au regard de la version 8 que le passage de la version 11 à 17 s'inscrit plutôt dans une continuité, avec un impact réduit sur les projets.

[EtudeSLL-OpenJDK17-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/355/download)

## Tableau de collecte de données
			
Les administrations sont régulièrement amenées à collecter des
données organisées en tableau et produites par un grand nombre
d'interlocuteurs. La circulation de tableaux bureautique, leur
remontée et leur exploitation entraînent de nombreuses
manipulations et potentiellement des erreurs. L'objet de cette
veille est d'identifier des solutions web permettant la collecte et
la valorisation des données.

Au côté de quelques solutions propriétaires évoquées, six
solutions sous licence libre ont été étudiées, BudiBase,
Baserow, NocoDB, SeaTable, JogetDX et Directus.

En l'état actuel, c'est Directus qui répond le mieux aux
exigences de l'administration. Il s'agit d'une solution mature,
avec une communauté active. Elle est facile à installer en
environnement cloud on-premise et propose un haut niveau
d'ergonomie. Les autres solutions plus récentes, un peu moins matures,
accusent quelques manques fonctionnels. Toutefois,
elles évoluent vite et pourraient gagner en intérêt dans les
années à venir.

[EtudeSLL-Collecte_de_donnees-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/350/download)
			
## Le poste de travail Linux

Les administrations utilisent le logiciel libre dans des
contextes de plus en plus variés et constatent l’intérêt
qu’elles peuvent en retirer en termes de souveraineté de
maîtrise technologique et de réduction des coûts. Il est
pourtant un domaine où le logiciel libre reste en retrait, c’est
celui du poste de l’agent. À de rares exceptions (la
gendarmerie), aucune administration n’a véritablement fait le
pas d’une migration complète vers un poste de travail Linux,
même si les logiciels libres Firefox, Thunderbird ou LibreOffice
sont très présents sur la poste de l’agent.

L'objet de cette étude est de dresser un état de l’art sur la
manière d’aborder un projet de transformation vers un poste de
travail Linux où la conduite du changement tient un rôle
primordial. 

L’approche de l’étude, plutôt stratégique, n’entre donc pas dans
les détails techniques d’une telle migration même si certains
points sont développés dans les annexes. De même, si la
problématique de la distribution Linux est évoquée, elle ne
donne pas lieu ici à une étude comparative des distributions
Linux. Enfin cette étude n’est pas une solution prête à l’emploi
pour une migration vers le poste Linux. Elle décrit simplement
dans les grandes lignes une démarche validée par l’expérience
qu'il conviendra d'ajuster selon les contextes.

L'étude se compose de quatre documents :
- **État de l’art et conduite du changement** : Point d'entrée de
  l'étude, il propose un rapide panorama des distributions Linux
  avant de dérouler une démarche globale de migration vers le
  poste Linux - Analyse du périmètre, stratégie entre migration,
  remédiation et statu quo, points d’attentions, conduite du
  projet, facteurs de réussite :

  [EtudeSLL-Poste de Travail Linux - Etat de l'art et conduite du changement-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/361/download)
- **Annexe Retours d’expérience** : Elle est constituée d’un
  entretien avec Nicolas Vivant qui a conduit les migrations
  vers le poste Linux pour les villes de Fontaine et maintenant
  d’Échirolles, puis un entretien avec Stéphane Dumond pour la
  gendarmerie : 
  
  [EtudeSLL- Poste de Travail Linux - Annexes - Retour Experience-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/360/download)
- **Annexe Gestion de parc** : Elle décrit de manière plus technique
  les moyens d’inventaires du parc informatique, préalable
  incontournable à tout projet de migration, ainsi que les
  outils de déploiement et gestion de parc nécessaires (OCS
  Inventory et Ansible) : 
  
  [EtudeSLL- Poste de Travail Linux - Annexes - Gestion Parc-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/359/download)
- **Annexe Accessibilité** : Elle aborde la question de
  l’accessibilité du poste Linux dans un contexte ou
  l’administration se doit d’être exemplaire : 
  
  [EtudeSLL- Poste de Travail Linux - Annexes - Accessiblité-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/358/download)

## Web Components

Le terme de "Web Components" désigne la spécification d'un
ensemble de technologies permettant la création de composants
encapsulés et réutilisables pour les applications web.

Cinq implémentations sont étudiées : Hybrids, LitElement,
Skate.js, Slim.js et Stencil

Deux implémentations LitElement et Stencil sortent du lot par
leur polyvalence sur l'ensemble des critères. Gage de pérennité,
l'une et l'autre sont soutenues par de grands acteurs, Google
pour la première et l’entreprise Ionic pour la seconde.

LitElement, grâce aux outils mis à disposition, propose une
courbe d'apprentissage facilitée, tandis que Stencil est le seul
à proposer une solution entreprise avec un support dédié.

Toutefois, malgré ses ambitions, cette technologie peine à
s'imposer. Ainsi élaborée sous l’égide du W3C, elle n'est
toujours pas un standard. Par ailleurs, l’utilisation du code
JavaScript pour les Web Components, amène des problématiques
d’accessibilité au regard du RGAA.

[EtudeSLL-Web_Components-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/363/download)

## Concentration des logs
Pour analyser le dysfonctionnement d'un service en ligne, des
traces horodatées sont nécessaires, c'est l'objet des
logs. Toutefois un service en ligne va mobiliser de nombreux
équipements et applications travaillant de concert, chacun
produisant des logs qu'il faut centraliser pour l'analyse
complète d'un dysfonctionnement. C'est l'objet des solutions de
centralisation des logs.

Les cinq solutions étudiées, Graylog, Logstash, Grafana Loky,
LogAgent et FluentDB sont équivalentes en termes de collecte
hétérogène de logs, de filtrage, de transformation et de
centralisation. Toutefois, LogAgent, et Fluentdb ne proposent
pas de fonctionnalités pour visualiser, rechercher ou analyser
les logs centralisés, ce qui nécessitera une solution tiers,
tandis que Graylog et Grafana Loki proposent intégrées ces
fonctionnalités. À noter que Grafana Loki ne fait pas
d'indexation plein texte, mais seulement sur les métadonnées,
les journaux étant compressés, non structurés, ce qui permet
d’économiser de l’espace disque et accélère les recherches.

À noter que le développement de LogAgent semble être au ralenti
depuis juillet 2021, ce qui interroge sur la pérennité de la
solution à moyen terme. D'autre part Graylog est couvert par la
licence Server Side Public License (SSPL) qui n'est pas reconnue
libre l'Open Source Initiative.

[EtudeSLL-Centralistation_des_Logs-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/349/download)

## Pare-feu applicatif

Un pare-feu applicatif (WAF - Web Application Firewall) est un
dispositif permettant de détecter et bloquer des attaques
potentielles provenant d'utilisateurs avant qu'elles
n'atteignent l'application. Il complète d’autres dispositifs
intervenant sous la couche applicative du modèle OSI.

Les offres propriétaires, cloud ou sous forme d'appliance sont
nombreuses tandis que les solutions WAF open source candidates à
un déploiement transversal dans de grandes organisations sont
rares. Beaucoup de projets sont abandonnés ou en cours
d'abandon, d'autres ne présentent pas le niveau de qualité ou de
pérennité attendu.

La solution historique ModSecurity, maintenant indépendante
(dans sa version 3) du serveur web Apache Httpd, offre un niveau
de protection générique contre les attaques les plus employées à
l'encontre des applications Web, grâce au «ModSecurity Core
Rule Set».

Le projet Vulture porté par la société française Advens semble
être la seule alternative aux leaders propriétaires. Il s'agit
d'une appliance de sécurité robuste et rapide construite
sur un système d'exploitation HardenedBSD. Il ne s'agit pas
encore d'une solution complète, mais la feuille de route là. 

[EtudeSLL-WAF-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/362/download)


## Alternative à Log4j

Log4J est une bibliothèque permettant de produire des logs (traces chronologiques) du fonctionnement d'une application Java. Ces traces sont utiles pour la mise au point des applications, pour auditer le fonctionnement d'une application et pour comprendre après coup d'éventuels dysfonctionnements.

Utiliser directement et indirectement de façon massif, la moindre faille de sécurité sur cette brique a un impact énorme sur la sécurité de l'ensemble des applications Java comme l'a montré la faille Log4Shell.

L'objet de l'étude est de faire un point sur Log4j et ses alternatives, principalement LogBack. Si cette dernière est techniquement au même niveau de maturité et fonctionnellement très proche de Log4j, sa pérennité est toutefois moins assurée, le projet étant porté essentiellement par une seul personne.  

Log4j de son côté bénéficie d'une large communauté (une centaine de développeurs) organisée et protégée par la fondation Apache, gage de stabilité et pérennité et qui a montré pour la faille	Log4Shell une très bonne réactivité. Log4j reste la meilleure solution de log en contexte Java. 

[EtudeSLL-Alternatives_Log4j-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/346/download)

## Messagerie asynchrone interapplicative 

Les échanges interapplicatifs, sans solution industrialisée,
constituent un point de fragilité important d'un SI
complexe. Les intergiciels "orientés message" (MOM), constituent
des solutions ouvertes, flexibles et extensibles à une diversité
de problèmes d’intégration en contexte hétérogène.

L’étude proposée compare trois solutions identifiées pour leur
qualité, robustesse et couverture fonctionnelle et adaptée à des
contextes d'utilisation spécifiques, il s’agit de ActiveMQ,
RabbitMQ et Kafka.

ActiveMQ est particulièrement indiqué pour mettre en oeuvre des
routages complexes de messages, mais au prix de performance
moindre en termes de débit ou de latence.

RabbitMQ constitue une sorte de compromis en permettant des
routages complexes, mais en préservant autant que possible une
faible latence.

Kafka va vraiment privilégier une faible latence et une grande
tenue à la charge. En contrepartie moins polyvalente, elle ne
permet pas des mécanismes complexes de routage.

[EtudeSLL-Messagerie_Asynchrone-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/354/download)

## Messagerie : Passerelles de filtrage

La messagerie est un point de vulnérabilité du SI qu'il convient
de protéger en vérifiant en particulier l'origine des messages
et en bloquant spam, fishing, virus. Face aux solutions
propriétaires, des alternatives Open source se détachent.

La première MailCleaner propose un ensemble relativement complet
de fonctionnalités. Toutefois, l'environnement d'administration
et l'accès à la liste des serveurs de spams maintenus en temps
réel (RBL/DNSBL) manquent à moins de souscrire à la version
Enterprise.

La deuxième, Proxmox Mail Gateway est une solution vraiment
complète qui propose des fonctionnalités avancées avec par
exemple la création de règles de gestion complexes, la haute
disponibilité et le pilotage via des API. Un support peut être
souscrit auprès de la société Proxmox pour un accès plus rapide
aux mises à jour.

[EtudeSLL-Passerelles-de-filtrage-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/357/download)

## Environnement de développement informatique

L'IDE (Integrated development environment) est le moyen
privilégié avec lequel le développeur crée et met au point les
programmes informatiques. Il est composé d'un ensemble très
large d'outils intégrés au sein d'une même interface.

Pour les développements Java, Eclipse, quoiqu'en perte de
vitesse, reste une référence devant Netbean et IntelliJ
Community Edition moins riche fonctionnellement. En revanche
pour le développement JavaScript, VSCodium et la référence
incontournable.

Il convient toutefois de surveiller la montée en puissance
d'Eclipse Theia/Che actuellement concurrent de VSCodium mais qui
pourrait à terme approcher la couverture d'Eclipse et ainsi
offrir une solution unique couvrant les développements Java et
JavaScript.

[EtudeSLL-IDEs-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/353/download)

## Orchestration de conteneurs
L'orchestration de conteneurs consiste à provisionner, monitorer
et gérer la montée en charge et la reprise sur erreur de
conteneurs déployés sur une infrastructure virtualisée.

L'étude propose un état des lieux des nombreux orchestrateurs
sous licence libre. Les solutions les plus faciles à intégrer
sont celles directement adossées à de grands éditeurs au prix
d'une certaine adhérence à leur écosystème. Il s'agit des
solutions Anthos, Tanzu avec cependant une préférence pour
OpenShift. Les solutions les plus agnostiques sont Rancher et
Kublr avec un certain retrait en terme fonctionnel toutefois.

Quoiqu'il en soit, la bonne solution est très dépendante du
contexte et des besoins, tandis qu'une bonne connaissance de
kubernetes est indispensable pour mener à bien la mise en oeuvre
d'un orchestrateur de conteneurs.

[EtudeSLL-Orchestration Conteneurs-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/356/download)


## Gestion de l'identité

La gestion des identités et des accès (GIA, ou IAM – Identity
and Access Management) est un élément essentiel de la sécurité
des systèmes d'information. Cette gestion comprend la création,
la modification, la suppression des comptes utilisateurs,
l'authentification (comment l'utilisateur prouve son identité),
les autorisations sur les différentes applications du SI et la
diffusion de l'identité au-delà avec les systèmes fédérés.

Cette étude couvre seulement le domaine du WebSSO qui concerne
les applications Web, ou les applications mobiles communiquant
sur le protocole HTTP.  Elle dresse un comparatif entre les deux
produits les plus déployés : LemonLDAP::NG et Keycloak. Le
premier se distinguant par sa large couverture fonctionnelle,
tandis que le second se distingue par sa facilité de mise en
œuvre.

[EtudeSLL-Authentification-unique-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/347/download)

<a id="orgedba060"></a>

## Etude centOS

La fin de vie prématurée de la distribution CentOS 8, annoncée
pour le 31 décembre 2021 et initialement prévue pour décembre
2029, remet en cause la stratégie de déploiement des
infrastructures utilisant la distribution CentOS.

Cette distribution est fortement utilisée dans certains
ministères avec un outillage d’industrialisation complexe
générant une adhérence forte. Son remplacement est donc
difficile à court terme et susceptible, à moyen terme, de
réorienter complément les technologies utilisées.

L’objet de l’étude est de faire un état des lieux des
distributions Linux disponible, de leurs points forts et leurs
points faibles dans le contexte des usages au sein des
administrations.

[EtudeSLL-CentOS-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/348/download)


<a id="orgd1a053a"></a>

## Logiciels de GMAO

La gestion et maintenance assistée par ordinateur consiste à
gérer des actifs matériels, leur localisation, le planning et
l’historique des interventions opérées. C’est un domaine peu
investi par le logiciel libre.

Néanmoins des solutions de CMDB ou de helpdesk comme Itop ou
GLPI sont susceptibles de couvrir une part des fonctionnalités
attendues, moyennant du paramétrage et quelques développements.

La solution la plus prometteuse reste OpenMAINT. Sa capacité à
répondre aux besoins ainsi que sa modularité héritée de CMDBuild
en font un outil viable qui demandera toutefois du paramétrage
particulièrement pour les fonctions de statistiques.

[EtudeSLL-GMAO-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/352/download)


<a id="org08c2889"></a>

## Espace de travail collaboratif

Le développement massif du télétravail sous la pression du
contexte sanitaire accélère les besoins en outils pour le
travail collaboratif.

Des solutions en ligne directement utilisable et très riche
fonctionnellement existent, cependant pour des raisons de
souveraineté, elles ne sont pas adaptées aux contraintes des
administrations.

Cette étude de veille s’intéresse donc aux alternatives sous
licence libre, que les administrations peuvent déployer sur
leurs propres serveurs, afin de recomposer des environnements de
travail collaboratif, proposant webconférence, tableau blanc,
partage d’écran, messagerie instantanée, vote en ligne, partage
de notes, partage de documents, édition collaborative.

L’étude montre que des solutions existent au prix d’un certain
travail d’intégration.

[EtudeSLL-Espace-de-travail-collaboratif-pub.odt](https://gitlab.adullact.net/marche-sll/etudes-de-veille/-/package_files/351/download)

